module code.vikunja.io/web

require (
	github.com/labstack/echo/v4 v4.1.7-0.20190627175217-8fb7b5be270f
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/tools v0.0.0-20190628034336-212fb13d595e // indirect
)
